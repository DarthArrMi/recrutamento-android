package com.dartharrmi.moviletrack;

import android.app.Application;

import com.dartharrmi.moviletrack.di.component.ApplicationComponent;
import com.dartharrmi.moviletrack.di.component.DaggerApplicationComponent;
import com.dartharrmi.moviletrack.di.module.ApplicationModule;

/**
 * Subclass of {@link Application} for dependency injection.
 */
public class MovileTraktApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    /**
     * To get ApplicationComponent
     *
     * @return ApplicationComponent
     */
    public ApplicationComponent getApplicationComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }
}
