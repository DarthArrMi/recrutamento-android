package com.dartharrmi.moviletrack.utils;

/**
 * MovileTrack
 * <p>
 * Created on 9/5/16.
 */
public enum ErrorCodes {

    CODE_400("Request couldn't be parsed"),
    CODE_401("Authentication problems."),
    CODE_403("Invalid API key or unapproved app"),
    CODE_404("Method exists, but no record found"),
    CODE_405("Method doesn't exist"),
    CODE_409("Resource already created"),
    CODE_412("Use application/json content type"),
    CODE_422("Invalid API key or unapproved app"),
    CODE_429("Rate Limit Exceeded"),
    CODE_500("Server Error"),
    CODE_503("Service Unavailable - server overloaded (try again in 30s)"),
    CODE_520("Service Unavailable - Cloudflare error");

    private final String errorMessage;

    ErrorCodes(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return this.errorMessage;
    }

    public static String getErrorMessage(int errorCode) {
        switch (errorCode) {
            case 400:
                return CODE_400.toString();
            case 401:
                return CODE_401.toString();
            case 403:
                return CODE_403.toString();
            case 404:
                return CODE_404.toString();
            case 405:
                return CODE_405.toString();
            case 409:
                return CODE_409.toString();
            case 412:
                return CODE_412.toString();
            case 422:
                return CODE_422.toString();
            case 429:
                return CODE_429.toString();
            case 500:
                return CODE_500.toString();
            case 503:
            case 504:
                return CODE_503.toString();
            case 520:
            case 521:
            case 522:
                return CODE_520.toString();
            default:
                return CODE_500.toString();
        }
    }
}
