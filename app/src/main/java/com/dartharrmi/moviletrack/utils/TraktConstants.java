package com.dartharrmi.moviletrack.utils;

/**
 * Useful constants for the app to work.
 */
public class TraktConstants {

    /**
     * Headers to be added in th request to Trakt API.
     */
    public static final class Headers {
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String TRAKT_API_VERSION = "trakt-api-version";
        public static final String TRAKT_API_KEY = "trakt-api-key";
    }

    /**
     * Values for the headers to be added in the request to Trakt API.
     */
    public static final class HeadersValues {
        public static final String APPLICATION_JSON = "application/json";
        public static final String API_VERSION_2 = "2";
        public static final String CLIENT_ID = "afe069d0d7569f89d673e4c69e06711cd5fe2f06258c2de21d18d086f319968c";
    }
}
