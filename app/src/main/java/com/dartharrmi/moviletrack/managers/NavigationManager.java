package com.dartharrmi.moviletrack.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;


/**
 * Class used to handle all the navigation done in the app.
 */
public class NavigationManager {

    public static void startActivity(Context context, Class targetActivity) {
        startActivity(context, targetActivity, null);
    }

    public static void startActivity(Context context, Class targetActivity, Bundle extras) {
        startActivity(context, targetActivity, extras, -1);
    }

    public static void startActivity(Fragment fragment, Class targetActivity, int requestCode, Bundle extras) {
        Intent intent = new Intent(fragment.getActivity(), targetActivity);
        intent.putExtras(extras);

        fragment.startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Context context, Class targetActivity, Bundle extras, int requestCode) {
        startActivity(context, targetActivity, extras, requestCode, null);
    }

    public static void startActivity(Context context, Class targetActivity, Bundle extras, int requestCode, int[] flags) {
        Intent intent = new Intent(context, targetActivity);

        if (extras != null) {
            intent.putExtras(extras);
        }

        if (flags != null && flags.length > 0) {
            for (Integer flag : flags) {
                intent.addFlags(flag);
            }
        }

        if (requestCode != -1) {
            ((Activity)context).startActivityForResult(intent, requestCode);
        } else {
            context.startActivity(intent);
        }
    }

    public static void navigateToFragment(@IdRes int containerId, FragmentActivity activity, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        fragmentManager.beginTransaction().add(containerId, fragment).commit();
    }

    public static void navigateToFragmentAddingToBackstack(@IdRes int containerId, String backstackKey,
                                                           FragmentActivity activity, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        fragmentManager.beginTransaction().add(containerId, fragment).addToBackStack(backstackKey).commit();
    }
}
