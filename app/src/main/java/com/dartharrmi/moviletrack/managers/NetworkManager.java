package com.dartharrmi.moviletrack.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.dartharrmi.moviletrack.MovileTraktApplication;
import com.dartharrmi.moviletrack.events.AndroidBus;
import com.dartharrmi.moviletrack.events.NetworkStateEvent;
import com.dartharrmi.moviletrack.events.SeasonEpisodesEvent;
import com.dartharrmi.moviletrack.events.ShowInfoEvent;
import com.dartharrmi.moviletrack.events.ShowSeasonsEvent;
import com.dartharrmi.moviletrack.models.Season;
import com.dartharrmi.moviletrack.models.SeasonEpisode;
import com.dartharrmi.moviletrack.models.Show;
import com.dartharrmi.moviletrack.utils.ErrorCodes;
import com.dartharrmi.moviletrack.utils.TraktConstants;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;


/**
 * Class in charge of handle all of the request to the Trakt API.
 */
@Singleton
public class NetworkManager {

    public static final String SCHEME = "https";
    public static final String HOST = "api.trakt.tv";
    public static final String SHOWS_SEGMENT = "shows";
    public static final String QUERY_PARAM_EXTENDED = "extended";
    public static final String QUERY_PARAM_EXTENDED_VALUE = "full,images";
    @Inject
    AndroidBus bus;

    Context context;
    Gson gson;
    OkHttpClient httpClient;

    private final String pathSegmentSeasons = "seasons";

    /**
     * Constructor.
     *
     * @param context {@link Context} instance to inject the dependencies.
     */
    public NetworkManager(Context context) {
        ((MovileTraktApplication)context).getApplicationComponent().inject(this);
        this.context = context;

        httpClient = new OkHttpClient();

        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(Show.class, new Show.ShowDeserializer())
                .registerTypeAdapter(Season.class, new Season.SeasonDeserializer())
                .registerTypeAdapter(SeasonEpisode.class, new SeasonEpisode.SeasonEpisodeDeserializer());
        gson = builder.create();
    }

    /**
     * Gets the information for a given show
     *
     * @param showName The show to be queried.
     */
    public void fetchShowInfo(@NonNull String showName) {
        if (!checkConnectivity(context)) {
            NetworkStateEvent networkStateEvent = new NetworkStateEvent(false);
            networkStateEvent.setShowAction(true);

            bus.post(networkStateEvent);
            return;
        }

        HttpUrl.Builder builder = new HttpUrl.Builder();
        String url = builder.scheme(SCHEME)
                .host(HOST)
                .addPathSegment(SHOWS_SEGMENT)
                .addPathSegment(showName)
                .addQueryParameter(QUERY_PARAM_EXTENDED, QUERY_PARAM_EXTENDED_VALUE)
                .build().toString();

        Request request = new Request.Builder()
                .addHeader(TraktConstants.Headers.CONTENT_TYPE, TraktConstants.HeadersValues.APPLICATION_JSON)
                .addHeader(TraktConstants.Headers.TRAKT_API_VERSION, TraktConstants.HeadersValues.API_VERSION_2)
                .addHeader(TraktConstants.Headers.TRAKT_API_KEY, TraktConstants.HeadersValues.CLIENT_ID)
                .url(url)
                .build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                ShowInfoEvent showInfoEvent = new ShowInfoEvent();
                showInfoEvent.setSuccess(false);
                showInfoEvent.setErrorMessage(ErrorCodes.getErrorMessage(500));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                ShowInfoEvent showInfoEvent = new ShowInfoEvent();

                if (response.code() >= 200 && response.code() < 300) {
                    Show show = gson.fromJson(response.body().string(), Show.class);
                    showInfoEvent.setShow(show);
                    showInfoEvent.setSuccess(true);
                } else {
                    showInfoEvent.setSuccess(false);
                    showInfoEvent.setErrorMessage(ErrorCodes.getErrorMessage(response.code()));
                }

                postShowInfoEvent(showInfoEvent);
            }
        });
    }

    /**
     * Gets the seasons for a given show.
     *
     * @param showName The show name.
     */
    public void fetchShowSeasons(@NonNull String showName) {
        if (!checkConnectivity(context)) {
            NetworkStateEvent networkStateEvent = new NetworkStateEvent(false);
            networkStateEvent.setShowAction(true);

            bus.post(networkStateEvent);
            return;
        }

        HttpUrl.Builder builder = new HttpUrl.Builder();
        String url = builder.scheme(SCHEME)
                .host(HOST)
                .addPathSegment(SHOWS_SEGMENT)
                .addPathSegment(showName)
                .addPathSegment(pathSegmentSeasons)
                .addQueryParameter(QUERY_PARAM_EXTENDED, QUERY_PARAM_EXTENDED_VALUE)
                .build().toString();

        Request request = new Request.Builder()
                .addHeader(TraktConstants.Headers.CONTENT_TYPE, TraktConstants.HeadersValues.APPLICATION_JSON)
                .addHeader(TraktConstants.Headers.TRAKT_API_VERSION, TraktConstants.HeadersValues.API_VERSION_2)
                .addHeader(TraktConstants.Headers.TRAKT_API_KEY, TraktConstants.HeadersValues.CLIENT_ID)
                .url(url)
                .build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                ShowInfoEvent showInfoEvent = new ShowInfoEvent();
                showInfoEvent.setSuccess(false);
                showInfoEvent.setErrorMessage(ErrorCodes.getErrorMessage(500));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                ShowSeasonsEvent showInfoEvent = new ShowSeasonsEvent();

                if (response.code() >= 200 && response.code() < 300) {
                    Type type = new TypeToken<ArrayList<Season>>() {
                    }.getType();

                    ArrayList<Season> seasons = gson.fromJson(response.body().string(), type);
                    showInfoEvent = new ShowSeasonsEvent();
                    showInfoEvent.setSuccess(true);
                    showInfoEvent.setSeasons(seasons);
                } else {
                    showInfoEvent.setSuccess(false);
                    showInfoEvent.setErrorMessage(ErrorCodes.getErrorMessage(response.code()));
                }

                postShowSeasonsEvent(showInfoEvent);
            }
        });
    }

    /**
     * Gets the episodes for a given season of a show.
     *
     * @param showName     The show name.
     * @param seasonNumber The season to be fetched.
     */
    public void fetchSeasonEpisodes(@NonNull String showName, @NonNull String seasonNumber) {
        if (!checkConnectivity(context)) {
            NetworkStateEvent networkStateEvent = new NetworkStateEvent(false);
            networkStateEvent.setShowAction(true);

            bus.post(networkStateEvent);
            return;
        }

        String url = new HttpUrl.Builder().scheme(SCHEME)
                .host(HOST)
                .addPathSegment(SHOWS_SEGMENT)
                .addPathSegment(showName)
                .addPathSegment(pathSegmentSeasons)
                .addPathSegment(seasonNumber)
                .build().toString();

        Request request = new Request.Builder()
                .addHeader(TraktConstants.Headers.CONTENT_TYPE, TraktConstants.HeadersValues.APPLICATION_JSON)
                .addHeader(TraktConstants.Headers.TRAKT_API_VERSION, TraktConstants.HeadersValues.API_VERSION_2)
                .addHeader(TraktConstants.Headers.TRAKT_API_KEY, TraktConstants.HeadersValues.CLIENT_ID)
                .url(url)
                .build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                ShowInfoEvent showInfoEvent = new ShowInfoEvent();
                showInfoEvent.setSuccess(false);
                showInfoEvent.setErrorMessage(ErrorCodes.getErrorMessage(500));
            }

            @Override
            public void onResponse(Response response) throws IOException {
                SeasonEpisodesEvent seasonEpisodesEvent = new SeasonEpisodesEvent();

                if (response.code() >= 200 && response.code() < 300) {
                    Type type = new TypeToken<ArrayList<SeasonEpisode>>() {
                    }.getType();

                    ArrayList<SeasonEpisode> seasonEpisodes = gson.fromJson(response.body().string(), type);
                    seasonEpisodesEvent.setSuccess(true);
                    seasonEpisodesEvent.setSeasonEpisodes(seasonEpisodes);
                } else {
                    seasonEpisodesEvent.setSuccess(false);
                    seasonEpisodesEvent.setErrorMessage(ErrorCodes.getErrorMessage(response.code()));
                }

                bus.post(seasonEpisodesEvent);
            }
        });
    }

    /**
     * Post an instance of {@link ShowInfoEvent} in the Bus. The event contains the show information.
     *
     * @param showInfoEvent The event.
     */
    private void postShowInfoEvent(ShowInfoEvent showInfoEvent) {
        bus.post(showInfoEvent);
    }

    /**
     * Post an instance of {@link ShowSeasonsEvent} in the Bus. The event contains the seasons of the
     * show.
     *
     * @param showSeasonsEvent The event.
     */
    private void postShowSeasonsEvent(ShowSeasonsEvent showSeasonsEvent) {
        bus.post(showSeasonsEvent);
    }

    /**
     * Post an instance of {@link SeasonEpisodesEvent} in the Bus. The event contains the episodes of
     * the season.
     *
     * @param seasonEpisodesEvent The event.
     */
    private void postSeasonEpisodesEvent(SeasonEpisodesEvent seasonEpisodesEvent) {
        bus.post(seasonEpisodesEvent);
    }

    /**
     * Checks if we have an active Internet connection before to attemp to make the request to the
     * Trakt API.
     *
     * @param context The Context.
     * @return true, if we have an active Internet connection, false otherwhise.
     */
    private boolean checkConnectivity(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
