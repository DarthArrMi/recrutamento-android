package com.dartharrmi.moviletrack.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dartharrmi.moviletrack.MovileTraktApplication;
import com.dartharrmi.moviletrack.events.AndroidBus;
import com.dartharrmi.moviletrack.events.NetworkStateEvent;

import javax.inject.Inject;

/**
 * Receiver to check whether the network is available or not.
 */
public class NetworkStateChangeReceiver extends BroadcastReceiver {

    @Inject
    AndroidBus bus;

    @Override
    public void onReceive(Context context, Intent intent) {
        ((MovileTraktApplication) context.getApplicationContext()).getApplicationComponent().inject(this);

        ConnectivityManager connectivityManager = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        bus.post(new NetworkStateEvent(activeNetworkInfo != null && activeNetworkInfo.isConnected()));
    }
}
