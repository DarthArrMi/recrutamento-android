package com.dartharrmi.moviletrack.adapters;

import com.dartharrmi.moviletrack.R;
import com.dartharrmi.moviletrack.models.Season;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Season's AAdapter used to show a list of seasons of a given show.
 */
public class SeasonsAdapter extends RecyclerView.Adapter<SeasonsAdapter.SeasonViewHolder> {

    private ArrayList<Season> seasons;
    private Context context;

    public SeasonsAdapter(@NonNull Context context) {
        this.context = context;
    }

    /**
     * Adds all the seasons to the adapter.
     *
     * @param seasons the seasons to add.
     */
    public void addSeasons(@NonNull ArrayList<Season> seasons) {
        if (this.seasons == null) {
            this.seasons = new ArrayList<>();
        }

        this.seasons.addAll(seasons);
        notifyDataSetChanged();
    }

    /**
     * Adds a single season to the adapter
     *
     * @param season The season to add.
     */
    public void addSeason(@NonNull Season season) {
        if (seasons == null) {
            seasons = new ArrayList<>();
        }

        seasons.add(season);
        notifyDataSetChanged();
    }

    /**
     * Gets the seasons at the given position.
     *
     * @param position The position.
     * @return The seaons at the given position, if the adapter is empty return null.
     */
    @Nullable
    public Season getSeason(int position) {
        if (seasons != null && !seasons.isEmpty()) {
            return seasons.get(position);
        }

        return null;
    }

    @Override
    public SeasonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_season_item, parent, false);

        return new SeasonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SeasonViewHolder holder, int position) {
        Season season = getSeason(position);

        if (season != null) {
            if (!TextUtils.isEmpty(season.getPoster().getThumb())) {
                Picasso.with(context).load(season.getPoster().getThumb())
                        .placeholder(R.drawable.serie_thumbnail_placeholder)
                        .error(R.drawable.serie_thumbnail_placeholder)
                        .fit().centerCrop().into(holder.imageViewSeasonThumb);
            }

            holder.textSeasonNumber.setText(season.getSeasonNumber());
            holder.textEpisodeCount.setText(context.getString(R.string.label_season_number_of_episodes, season.getEpisodeCount()));
            holder.textSeasonRating.setText(context.getString(R.string.label_season_rating, season.getRating()));
        }
    }

    @Override
    public int getItemCount() {
        return seasons != null ? seasons.size() : 0;
    }

    /**
     * ViewHolder for the seasons.
     */
    public static class SeasonViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewSeasonThumb;

        @Bind(R.id.text_season_number)
        TextView textSeasonNumber;

        @Bind(R.id.text_season_episode_count)
        TextView textEpisodeCount;

        @Bind(R.id.text_season_rating)
        TextView textSeasonRating;

        public SeasonViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            imageViewSeasonThumb = ButterKnife.findById(itemView, R.id.imageview_season_thumb);
        }
    }
}
