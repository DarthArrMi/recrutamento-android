package com.dartharrmi.moviletrack.adapters;

import com.dartharrmi.moviletrack.R;
import com.dartharrmi.moviletrack.models.SeasonEpisode;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Episodes adapter to show a list of episodes of a given season.
 */
public class SeasonEpisodeAdapter extends RecyclerView.Adapter<SeasonEpisodeAdapter.SeasonEpisodeViewHolder> {

    private Context context;
    private ArrayList<SeasonEpisode> episodes;

    public SeasonEpisodeAdapter(Context context) {
        this.context = context;
    }

    /**
     * Adds a single episode to the adapter.
     *
     * @param seasonEpisode The Episode to add.
     */
    public void addSeasonEpisodes(@NonNull SeasonEpisode seasonEpisode) {
        if (episodes == null) {
            episodes = new ArrayList<>();
        }

        episodes.add(seasonEpisode);
        notifyDataSetChanged();
    }

    /**
     * Adds all the episodes to the adapter.
     *
     * @param seasonEpisodes The episodes to add.
     */
    public void addSeasonEpisodes(@NonNull ArrayList<SeasonEpisode> seasonEpisodes) {
        if (episodes == null) {
            episodes = new ArrayList<>();
        }

        episodes.addAll(seasonEpisodes);
        notifyDataSetChanged();
    }

    /**
     * Returns the episode at the position.
     *
     * @param position The position
     * @return The episode at the position, if the adapter is empty return null.
     */
    @Nullable
    public SeasonEpisode getSeasonEpisode(int position) {
        return episodes != null ? episodes.get(position) : null;
    }

    @Override
    public SeasonEpisodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_episode_item, parent, false);

        return new SeasonEpisodeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SeasonEpisodeViewHolder holder, int position) {
        SeasonEpisode episode = episodes.get(position);

        if (episode != null) {
            holder.textSeasonEpisodeNumber.setText(episode.getEpisodeNumber());
            holder.textSeasonEpisodeName.setText(episode.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return episodes != null ? episodes.size() : 0;
    }

    /**
     * ViewHolder for the episodes.
     */
    public static class SeasonEpisodeViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.season_episode_number)
        TextView textSeasonEpisodeNumber;

        @Bind(R.id.season_episode_name)
        TextView textSeasonEpisodeName;

        public SeasonEpisodeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
