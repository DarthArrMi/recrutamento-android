package com.dartharrmi.moviletrack.di.component;

import com.dartharrmi.moviletrack.MovileTraktApplication;
import com.dartharrmi.moviletrack.di.module.ApplicationModule;
import com.dartharrmi.moviletrack.managers.NetworkManager;
import com.dartharrmi.moviletrack.receiver.NetworkStateChangeReceiver;
import com.dartharrmi.moviletrack.ui.activities.MainActivity;
import com.dartharrmi.moviletrack.ui.activities.SeasonEpisodesActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Application component for dependency injection with Dagger.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(MovileTraktApplication movileTraktApplication);

    void inject(MainActivity baseActivity);

    void inject(SeasonEpisodesActivity seasonEpisodesActivity);

    void inject(NetworkStateChangeReceiver networkStateChangeReceiver);

    void inject(NetworkManager networkManager);

    NetworkManager provideNetworkManager();
}
