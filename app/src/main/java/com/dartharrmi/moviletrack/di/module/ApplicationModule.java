package com.dartharrmi.moviletrack.di.module;

import android.app.Application;
import android.content.Context;

import com.dartharrmi.moviletrack.events.AndroidBus;
import com.dartharrmi.moviletrack.managers.NetworkManager;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Application Module for dependency injection with Dagger.
 */
@Module
public class ApplicationModule {

    protected final Application application;

    /**
     * Constructor.
     *
     * @param application Current Application running.
     */
    public ApplicationModule(Application application) {
        this.application = application;
    }

    /**
     * Provides an {@link Application} instance.
     *
     * @return An application instance.
     */
    @Provides
    Application provideApplication() {
        return application;
    }

    /**
     * Provides an {@link Bus} instance, this object will be used to post and receive events in the
     * differents components of the app.
     *
     * @return And instance of {@link Bus}.
     */
    @Provides
    @Singleton
    AndroidBus provideBus() {
        return new AndroidBus(ThreadEnforcer.ANY);
    }

    /**
     * Provides an {@link NetworkManager} instance.
     *
     * @param bus The {@link Bus} instance used to send events to the UI components.
     * @return An instance of {@link NetworkManager}.
     */
    @Provides
    @Singleton
    NetworkManager provideNetworkManager() {
        return new NetworkManager(application);
    }
}
