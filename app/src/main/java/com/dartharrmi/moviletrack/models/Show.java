package com.dartharrmi.moviletrack.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 *
 */
public class Show {

    private double rating;
    private int airedEpisodes;
    private int year;
    private String title;
    private String overview;
    private String network;
    private Poster poster;

    public Show() {
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getAiredEpisodes() {
        return airedEpisodes;
    }

    public void setAiredEpisodes(int airedEpisodes) {
        this.airedEpisodes = airedEpisodes;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public Poster getPoster() {
        return poster;
    }

    public void setPoster(Poster poster) {
        this.poster = poster;
    }

    public static class ShowDeserializer implements JsonDeserializer<Show> {

        @Override
        public Show deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = json.getAsJsonObject();

            Show show = new Show();
            show.setAiredEpisodes(jsonObject.get("aired_episodes").getAsInt());
            show.setNetwork(jsonObject.get("network").getAsString());
            show.setOverview(jsonObject.get("overview").getAsString());
            show.setRating(jsonObject.get("rating").getAsDouble());
            show.setTitle(jsonObject.get("title").getAsString());
            show.setYear(jsonObject.get("year").getAsInt());

            JsonObject imagesObject = jsonObject.getAsJsonObject("images");
            JsonObject posterNode = imagesObject.getAsJsonObject("poster");

            Poster poster = new Poster();
            poster.setFull(posterNode.get("full").getAsString());
            poster.setMedium(posterNode.get("medium").getAsString());
            poster.setThumb(posterNode.get("thumb").getAsString());
            show.setPoster(poster);

            return show;
        }
    }
}
