package com.dartharrmi.moviletrack.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * MovileTrack
 * <p>
 * Created on 9/5/16.
 */
public class Season implements Parcelable {

    private String seasonNumber;
    private double rating;
    private int episodeCount;
    private Poster poster;

    public Season() {
    }

    protected Season(Parcel in) {
        seasonNumber = in.readString();
        rating = in.readDouble();
        episodeCount = in.readInt();
        poster = in.readParcelable(Poster.class.getClassLoader());
    }

    public static final Creator<Season> CREATOR = new Creator<Season>() {
        @Override
        public Season createFromParcel(Parcel in) {
            return new Season(in);
        }

        @Override
        public Season[] newArray(int size) {
            return new Season[size];
        }
    };

    public String getSeason() {
        return seasonNumber;
    }

    public String getSeasonNumber() {
        return "Season " + seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public Poster getPoster() {
        return poster;
    }

    public void setPoster(Poster poster) {
        this.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(seasonNumber);
        dest.writeDouble(rating);
        dest.writeInt(episodeCount);
        dest.writeParcelable(poster, flags);
    }

    public static class SeasonDeserializer implements JsonDeserializer<Season> {

        @Override
        public Season deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject jsonObject = json.getAsJsonObject();

            Season show = new Season();
            show.setSeasonNumber(String.format("%1$d", jsonObject.get("number").getAsInt()));
            show.setRating(jsonObject.get("rating").getAsDouble());
            show.setEpisodeCount(jsonObject.get("episode_count").getAsInt());

            JsonObject imagesObject = jsonObject.getAsJsonObject("images");
            JsonObject posterNode = imagesObject.getAsJsonObject("poster");

            Poster poster = new Poster();
            poster.setFull(!(posterNode.get("full") instanceof JsonNull) ? posterNode.get("full").getAsString() : "");
            poster.setMedium(!(posterNode.get("medium") instanceof JsonNull) ? posterNode.get("medium").getAsString() : "");
            poster.setThumb(!(posterNode.get("thumb") instanceof JsonNull) ? posterNode.get("thumb").getAsString() : "");
            show.setPoster(poster);

            return show;
        }
    }
}
