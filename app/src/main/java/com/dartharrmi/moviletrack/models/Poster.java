package com.dartharrmi.moviletrack.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * MovileTrack
 * <p/>
 * Created on 9/5/16.
 */
public class Poster implements Parcelable {

    private String full;
    private String medium;
    private String thumb;

    public Poster() {
    }

    public Poster(String full, String medium, String thumb) {
        this.full = full;
        this.medium = medium;
        this.thumb = thumb;
    }

    protected Poster(Parcel in) {
        full = in.readString();
        medium = in.readString();
        thumb = in.readString();
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    // region Parcelable Implementation
    public static final Creator<Poster> CREATOR = new Creator<Poster>() {
        @Override
        public Poster createFromParcel(Parcel in) {
            return new Poster(in);
        }

        @Override
        public Poster[] newArray(int size) {
            return new Poster[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(full);
        dest.writeString(medium);
        dest.writeString(thumb);
    }
    //endregion
}
