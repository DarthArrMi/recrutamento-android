package com.dartharrmi.moviletrack.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * MovileTrack
 * <p>
 * Created on 9/6/16.
 */
public class SeasonEpisode {

    private String episodeNumber;
    private String title;

    public SeasonEpisode() {
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static class SeasonEpisodeDeserializer implements JsonDeserializer<SeasonEpisode> {

        @Override
        public SeasonEpisode deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            final JsonObject seasonEpisodeJsonObject = json.getAsJsonObject();
            SeasonEpisode episode = new SeasonEpisode();

            episode.setEpisodeNumber(String.format("E%1$d", seasonEpisodeJsonObject.get("number").getAsInt()));
            episode.setTitle(seasonEpisodeJsonObject.get("title").getAsString());

            return episode;
        }
    }
}
