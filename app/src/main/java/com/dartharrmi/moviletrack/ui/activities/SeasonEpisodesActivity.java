package com.dartharrmi.moviletrack.ui.activities;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dartharrmi.moviletrack.MovileTraktApplication;
import com.dartharrmi.moviletrack.R;
import com.dartharrmi.moviletrack.adapters.SeasonEpisodeAdapter;
import com.dartharrmi.moviletrack.events.AndroidBus;
import com.dartharrmi.moviletrack.events.SeasonEpisodesEvent;
import com.dartharrmi.moviletrack.managers.NetworkManager;
import com.dartharrmi.moviletrack.models.Season;
import com.dartharrmi.moviletrack.models.SeasonEpisode;
import com.dartharrmi.moviletrack.ui.HorizontalItemDecoration;
import com.dartharrmi.moviletrack.utils.Transformations;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * MovileTrack
 * <p/>
 * Created on 9/6/16.
 */
public class SeasonEpisodesActivity extends AppCompatActivity {

    public static final String EXTRA_SEASON = "EXTRA_SEASON";

    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    @Bind(R.id.imageview_show_header)
    ImageView imageViewHeader;

    @Bind(R.id.imageview_show_thumb)
    ImageView imageViewThumb;

    @Bind(R.id.container_header)
    View containerHeader;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.recyclerview_list)
    RecyclerView recyclerViewEpisodes;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    AndroidBus bus;

    @Inject
    NetworkManager networkManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);

        ((MovileTraktApplication) getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);
        bus.register(this);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) imageViewThumb.getLayoutParams();
        params.setBehavior(null);
        imageViewThumb.setLayoutParams(params);
        imageViewThumb.setVisibility(View.GONE);
        containerHeader.setVisibility(View.GONE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewEpisodes.setHasFixedSize(true);
        recyclerViewEpisodes.setLayoutManager(linearLayoutManager);
        recyclerViewEpisodes.addItemDecoration(new HorizontalItemDecoration(getResources()));
        recyclerViewEpisodes.setNestedScrollingEnabled(false);

        toolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        toolbarLayout.setCollapsedTitleTextColor(getResources().getColor(android.R.color.white));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Season season = extras.getParcelable(EXTRA_SEASON);

            if (season != null) {
                toolbarLayout.setTitle(season.getSeasonNumber());

                if (!TextUtils.isEmpty(season.getPoster().getMedium())) {
                    Picasso.with(getApplicationContext())
                            .load(season.getPoster().getMedium())
                            .placeholder(R.drawable.serie_thumbnail_placeholder)
                            .error(R.drawable.serie_thumbnail_placeholder)
                            .fit().centerCrop()
                            .transform(Transformations.PaletteTransformation.instance())
                            .into(imageViewHeader, new Callback() {
                                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                                @Override
                                public void onSuccess() {
                                    Bitmap bitmap = ((BitmapDrawable) imageViewHeader.getDrawable()).getBitmap();
                                    Palette palette = Transformations.PaletteTransformation.getPalette(bitmap);

                                    if (palette != null) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            getWindow().setStatusBarColor(palette.getDarkVibrantColor(getResources().getColor(R.color.colorPrimaryDark)));
                                        }
                                        toolbarLayout.setContentScrimColor(palette.getDarkVibrantColor(getResources().getColor(R.color.colorPrimaryDark)));
                                    }
                                }

                                @Override
                                public void onError() {
                                }
                            });
                }

                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(true);
                networkManager.fetchSeasonEpisodes(getString(R.string.show_slug), season.getSeason());
            }
        }
    }

    @Subscribe
    public void onFetchSeasonEpisodes(SeasonEpisodesEvent seasonEpisodesEvent) {
        progressBar.setVisibility(View.GONE);

        if (seasonEpisodesEvent.isSuccess()) {
            ArrayList<SeasonEpisode> episodes = seasonEpisodesEvent.getSeasonEpisodes();

            SeasonEpisodeAdapter adapter = new SeasonEpisodeAdapter(getApplicationContext());
            adapter.addSeasonEpisodes(episodes);
            recyclerViewEpisodes.setAdapter(adapter);
        } else {
            Snackbar.make(recyclerViewEpisodes, seasonEpisodesEvent.getErrorMessage(), Snackbar.LENGTH_LONG).show();
        }
    }
}
