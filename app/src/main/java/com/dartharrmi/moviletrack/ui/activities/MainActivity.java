package com.dartharrmi.moviletrack.ui.activities;

import com.dartharrmi.moviletrack.MovileTraktApplication;
import com.dartharrmi.moviletrack.R;
import com.dartharrmi.moviletrack.adapters.SeasonsAdapter;
import com.dartharrmi.moviletrack.events.AndroidBus;
import com.dartharrmi.moviletrack.events.NetworkStateEvent;
import com.dartharrmi.moviletrack.events.ShowInfoEvent;
import com.dartharrmi.moviletrack.events.ShowSeasonsEvent;
import com.dartharrmi.moviletrack.listener.RecyclerViewItemClickListener;
import com.dartharrmi.moviletrack.managers.NavigationManager;
import com.dartharrmi.moviletrack.managers.NetworkManager;
import com.dartharrmi.moviletrack.models.Season;
import com.dartharrmi.moviletrack.models.Show;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    @Bind(R.id.imageview_show_header)
    ImageView imageViewHeader;

    @Bind(R.id.imageview_show_thumb)
    ImageView imageViewThumb;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.recyclerview_list)
    RecyclerView recyclerViewSeasons;

    @Bind(R.id.text_show_rating)
    TextView textShowRating;

    @Bind(R.id.text_show_number_of_episodes)
    TextView textShowNumberOfEpisodes;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.coordinator)
    View coordinator;

    @Inject
    AndroidBus bus;

    @Inject
    NetworkManager networkManager;

    private SeasonsAdapter seasonsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);

        ((MovileTraktApplication)getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);
        bus.register(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewSeasons.setHasFixedSize(true);
        recyclerViewSeasons.setLayoutManager(linearLayoutManager);
        recyclerViewSeasons.setNestedScrollingEnabled(false);
        recyclerViewSeasons.addOnItemTouchListener(new RecyclerViewItemClickListener(this, new RecyclerViewItemClickListener.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Season season = seasonsAdapter.getSeason(position);

                Bundle extras = new Bundle();
                extras.putParcelable(SeasonEpisodesActivity.EXTRA_SEASON, season);

                NavigationManager.startActivity(MainActivity.this, SeasonEpisodesActivity.class, extras);
            }
        }));

        toolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        toolbarLayout.setCollapsedTitleTextColor(getResources().getColor(android.R.color.white));

        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        networkManager.fetchShowInfo(getString(R.string.show_slug));
    }

    //region Events Callbacks
    @Subscribe
    public void onNetworkStateChanged(NetworkStateEvent networkStateEvent) {
        progressBar.setVisibility(View.GONE);

        if (networkStateEvent.isConnected()) {
            Snackbar.make(coordinator, getString(R.string.label_snack_internet_available), Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar snackbar = Snackbar.make(coordinator, getString(R.string.label_snack_internet_not_available), Snackbar.LENGTH_INDEFINITE);
            if (networkStateEvent.isShowAction()) {
                snackbar.setAction(getString(R.string.label_snack_retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressBar.setVisibility(View.VISIBLE);
                        networkManager.fetchShowInfo(getString(R.string.show_slug));
                        networkManager.fetchShowSeasons(getString(R.string.show_slug));
                    }
                }).show();
            }
        }
    }

    @Subscribe
    public void onFetchShowInfo(ShowInfoEvent showInfoEvent) {
        if (showInfoEvent.isSuccess()) {
            Show show = showInfoEvent.getShow();

            textShowRating.setText(String.format("%.1f", show.getRating()));
            textShowNumberOfEpisodes.setText(getString(R.string.label_show_number_of_episodes, show.getAiredEpisodes()));

            Picasso.with(getApplicationContext())
                    .load(show.getPoster().getMedium())
                    .placeholder(R.drawable.season_background_placeholder)
                    .error(R.drawable.season_background_placeholder)
                    .fit().centerCrop()
                    .into(imageViewHeader);
            Picasso.with(getApplicationContext())
                    .load(show.getPoster().getThumb())
                    .placeholder(R.drawable.serie_thumbnail_placeholder)
                    .error(R.drawable.serie_thumbnail_placeholder)
                    .fit().centerCrop()
                    .into(imageViewThumb);

            networkManager.fetchShowSeasons(getString(R.string.show_slug));
        } else {
            Snackbar.make(coordinator, showInfoEvent.getErrorMessage(), Snackbar.LENGTH_LONG).show();
        }
    }

    @Subscribe
    public void onFetchShowSeasons(ShowSeasonsEvent event) {
        progressBar.setVisibility(View.GONE);

        if (event.isSuccess()) {
            ArrayList<Season> seasons = event.getSeasons();

            seasonsAdapter = new SeasonsAdapter(this);
            seasonsAdapter.addSeasons(seasons);
            recyclerViewSeasons.setAdapter(seasonsAdapter);
        } else {
            Snackbar.make(coordinator, event.getErrorMessage(), Snackbar.LENGTH_LONG).show();
        }
    }
    //endregion
}
