package com.dartharrmi.moviletrack.events;

import com.dartharrmi.moviletrack.models.Season;

import java.util.ArrayList;

/**
 * MovileTrack
 * <p/>
 * Created on 9/4/16.
 */
public class ShowSeasonsEvent {

    private boolean success;
    private String errorMessage;
    private ArrayList<Season> seasons;

    public ShowSeasonsEvent() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(ArrayList<Season> seasons) {
        this.seasons = seasons;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
