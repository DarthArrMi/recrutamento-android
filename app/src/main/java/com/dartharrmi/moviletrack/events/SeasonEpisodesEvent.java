package com.dartharrmi.moviletrack.events;

import com.dartharrmi.moviletrack.models.SeasonEpisode;

import java.util.ArrayList;

/**
 * MovileTrack
 * <p>
 * Created on 9/6/16.
 */
public class SeasonEpisodesEvent {

    private boolean success;
    private String errorMessage;
    private ArrayList<SeasonEpisode> seasonEpisodes;

    public SeasonEpisodesEvent() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<SeasonEpisode> getSeasonEpisodes() {
        return seasonEpisodes;
    }

    public void setSeasonEpisodes(ArrayList<SeasonEpisode> seasonEpisodes) {
        this.seasonEpisodes = seasonEpisodes;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
