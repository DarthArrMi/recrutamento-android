package com.dartharrmi.moviletrack.events;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * MovileTrack
 * <p/>
 * Created on 9/5/16.
 */
public class AndroidBus extends Bus {

    private Handler mainThread = new Handler(Looper.getMainLooper());

    public AndroidBus(ThreadEnforcer enforcer) {
        super(enforcer);
    }

    @Override
    public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    post(event);
                }
            });
        }
    }
}
