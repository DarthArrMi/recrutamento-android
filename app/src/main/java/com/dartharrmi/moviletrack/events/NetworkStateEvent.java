package com.dartharrmi.moviletrack.events;

/**
 * MovileTrack
 * <p/>
 * Created on 9/4/16.
 */
public class NetworkStateEvent {

    private boolean connected;
    private boolean showAction;

    public NetworkStateEvent(boolean connected) {
        this.connected = connected;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public boolean isShowAction() {
        return showAction;
    }

    public void setShowAction(boolean showAction) {
        this.showAction = showAction;
    }
}
