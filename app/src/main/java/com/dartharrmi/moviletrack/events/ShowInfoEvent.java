package com.dartharrmi.moviletrack.events;

import com.dartharrmi.moviletrack.models.Show;

/**
 * MovileTrack
 * <p>
 * Created on 9/4/16.
 */
public class ShowInfoEvent {

    private boolean success;
    private String errorMessage;
    private Show show;

    public ShowInfoEvent() {
    }

    public ShowInfoEvent(Show show) {
        this.show = show;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
