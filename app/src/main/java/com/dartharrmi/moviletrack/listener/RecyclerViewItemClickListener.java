package com.dartharrmi.moviletrack.listener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;


/**
 * Listener to know which item was tapped in a {@link RecyclerView}.
 */
public class RecyclerViewItemClickListener implements RecyclerView.OnItemTouchListener {

    /**
     * Interface to handle a selection in a {@link RecyclerView}.
     */
    public interface OnRecyclerViewItemClickListener {

        void onItemClick(View view, int position);
    }

    private GestureDetector mGestureDetector;
    private OnRecyclerViewItemClickListener singleClickListener;

    public RecyclerViewItemClickListener(Context context, OnRecyclerViewItemClickListener listener) {
        singleClickListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null && singleClickListener != null && mGestureDetector.onTouchEvent(e)) {
            singleClickListener.onItemClick(child, rv.getChildPosition(child));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }
}
